import { Link, graphql, useStaticQuery } from "gatsby"
import React from "react"
// import './header.module.scss'
import headerStyles from './header.module.scss'

const Header = () => {
  // look for siteMetadata in gatsby-config with graphql
  const data = useStaticQuery(graphql`
  query {
    site {
      siteMetadata {
        title
        description
      }
    }
  }
  `)
  return (
  <header className={headerStyles.header}>
  <div>
  <div className={headerStyles.topHeader}>
  <h1 >
    <Link  className={headerStyles.title} to="/"> 
    {data.site.siteMetadata.title} 
    </Link>
  </h1>
  <p>    {data.site.siteMetadata.description} 
</p>
</div>
  <nav>
    <ul className={headerStyles.navList}>

      <li>
        <Link className={headerStyles.navItem} 
        activeClassName={headerStyles.activeNavItem} 
        to="/">Accueil</Link>
      </li>

    <li><Link className={headerStyles.navItem} 
    activeClassName={headerStyles.activeNavItem} 
    to="/caseStudy">Etudes de cas</Link> </li>

    <li><Link className={headerStyles.navItem} 
    activeClassName={headerStyles.activeNavItem} 
    to="/about">A propos</Link> </li>

    <li><Link className={headerStyles.navItem} 
    activeClassName={headerStyles.activeNavItem} 
    to="/simulation">Simulation</Link> </li>

    <li><Link className={headerStyles.navItem} 
    activeClassName={headerStyles.activeNavItem} 
    to="/contact">Nous contacter</Link> </li>

    </ul>
    </nav>
  </div>

  </header>
  )
}

export default Header
