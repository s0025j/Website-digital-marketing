import React from "react"
import { Link, graphql, useStaticQuery } from "gatsby"
import footerStyles from './footer.module.scss'

// add three columns plus social media + "quote" + logo


const Footer = () => {
      // look for siteMetadata in gatsby-config with graphql
    const data = useStaticQuery(graphql`
    query {
      site {
        siteMetadata {
          author
        }
      }
    }
    `)
    return (
        <footer className={footerStyles.footer}>
        <div className={footerStyles.inlineFooter}>
            <p>Created by {data.site.siteMetadata.author}, 2020</p>
            <p> <Link to="/CGU">CGU</Link> </p>
            <p> <Link to="/PolitiqueC">Politique de confidentialité</Link> </p>
            </div>
        </footer>
    )
}

export default Footer