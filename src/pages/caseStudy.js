import React from 'react'
import Layout from '../components/layout'
import { Link, graphql, useStaticQuery } from "gatsby"

import blogStyles from './caseStudy.module.scss'

const Blog = () => {
    const data = useStaticQuery(graphql`
    query {
        allMarkdownRemark {
          edges {
            node {
              frontmatter {
                title
                date
              }
              fields {
                slug
            }
              html
              excerpt
            }
          }
        }
    }
  `)

    return (
        <Layout>
            <h1>Etudes de cas</h1>
                <ol className={blogStyles.posts}>
                {data.allMarkdownRemark.edges.map((edge)=> {
                    return(
                        <li className={blogStyles.post}>
                            <Link to={edge.node.fields.slug}><h2>{edge.node.frontmatter.title}</h2></Link>
                            <p>{edge.node.frontmatter.date}</p>
                        </li>
                    )
                })}
                </ol>
                </Layout>

            
    )
}

export default Blog